const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');

const extractLess = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: './index.js',
    output: {
        path: __dirname + '/dist',
        filename: '[name].js',
        chunkFilename: "[id].js"
    },
    node: {fs: "empty"},
    module: {
        rules: [{
                test: /\.js$/,
                exclude: '/node_modules/',
                loader: 'babel-loader',
                query: {
                    presets: ['es2015']
                },
            },{
                test: /\.(css|less)$/,
                loader: ExtractTextPlugin.extract("css-loader", "less-loader")
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin("[name].css"), 
        new HtmlWebpackPlugin({
            template: 'src/index.pug'
        })
    ]
};